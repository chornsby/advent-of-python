from setuptools import find_packages, setup

setup(
    name="advent-of-python",
    version="1.0.0",
    package_dir={"": "src"},
    packages=find_packages("src"),
    install_requires=["Click==7.0", "more-itertools==8.0.2"],
    tests_require=["pytest"],
    entry_points={"console_scripts": ["advent-of-python=advent.cli:cli"]},
)
