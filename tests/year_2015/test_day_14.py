import textwrap

import pytest

from advent.year_2015 import day_14

CONTENT = textwrap.dedent(
    """
    Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
    Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
    """
).strip()


@pytest.mark.parametrize(
    "duration,expected", ((1, 16), (10, 160), (11, 176), (1000, 1120))
)
def test_calculate_winning_distance(duration, expected):
    assert day_14.calculate_winning_distance(CONTENT, duration) == expected


@pytest.mark.parametrize(
    "duration,expected", ((1, 1), (140, 139), (1000, 689))
)
def test_calculate_winning_points(duration, expected):
    assert day_14.calculate_winning_points(CONTENT, duration) == expected
