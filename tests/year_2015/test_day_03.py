import pytest

from advent.year_2015 import day_03


@pytest.mark.parametrize(
    "content,num_santas,expected",
    (
        (">", 1, 2),
        ("^>v<", 1, 4),
        ("^v^v^v^v^v", 1, 2),
        ("^v", 2, 3),
        ("^>v<", 2, 3),
        ("^v^v^v^v^v", 2, 11),
    ),
)
def test_count_present_deliveries(content, num_santas, expected):
    assert day_03.count_present_deliveries(content, num_santas=num_santas) == expected
