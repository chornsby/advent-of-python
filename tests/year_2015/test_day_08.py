import pytest

from advent.year_2015 import day_08


@pytest.mark.parametrize(
    "content,expected", ((r'""', 2), (r'"abc"', 2), (r'"aaa\"aaa"', 3), (r'"\x27"', 5))
)
def test_calculate_decode_difference(content, expected):
    assert day_08.calculate_decode_difference(content) == expected


@pytest.mark.parametrize(
    "content,expected", ((r'""', 4), (r'"abc"', 4), (r'"aaa\"aaa"', 6), (r'"\x27"', 5))
)
def test_calculate_encode_difference(content, expected):
    assert day_08.calculate_encode_difference(content) == expected
