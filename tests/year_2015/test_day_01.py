import pytest

from advent.year_2015 import day_01


@pytest.mark.parametrize(
    "content,expected",
    (
        ("(())", 0),
        ("()()", 0),
        ("(((", 3),
        ("(()(()(", 3),
        ("))(((((", 3),
        ("())", -1),
        ("))(", -1),
        (")))", -3),
        (")())())", -3),
    ),
)
def test_find_destination_floor(content, expected):
    assert day_01.find_destination_floor(content) == expected


@pytest.mark.parametrize(
    "content,expected", ((")", 1), ("()())", 5)),
)
def test_find_basement_index(content, expected):
    assert day_01.find_basement_index(content) == expected
