import textwrap

from advent.year_2015 import day_15

CONTENT = textwrap.dedent(
    """
    Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
    Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3
    """
).strip()


def test_find_optimal_cookie_score():
    assert day_15.find_optimal_cookie_score(CONTENT) == 62_842_880


def test_find_optimal_cookie_score_with_500_calories():
    assert day_15.find_optimal_cookie_score(CONTENT, require_calories=500) == 57_600_000
