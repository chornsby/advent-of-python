import pytest

from advent.year_2015 import day_10


@pytest.mark.parametrize(
    "number,repetitions,expected",
    ((1, 1, 2), (11, 1, 2), (21, 1, 4), (1211, 1, 6), (111221, 1, 6), (1, 4, 6)),
)
def test_look_and_say(number, repetitions, expected):
    assert day_10.look_and_say(number, repetitions) == expected
