import pytest

from advent.year_2015 import day_04


@pytest.mark.parametrize(
    "secret_key,leading_zeroes,expected",
    (("abcdef", 5, 609043), ("pqrstuv", 5, 1048970)),
)
def test_mine_advent_coin(secret_key, leading_zeroes, expected):
    assert day_04.mine_advent_coin(secret_key, leading_zeroes) == expected
