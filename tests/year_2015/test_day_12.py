import pytest

from advent.year_2015 import day_12


@pytest.mark.parametrize(
    "content,expected",
    (
        ([1, 2, 3], 6),
        ({"a": 2, "b": 4}, 6),
        ([[[3]]], 3),
        ({"a": {"b": 4}, "c": -1}, 3),
        ({"a": [-1, 1]}, 0),
        ([-1, {"a": 1}], 0),
        ([], 0),
        ({}, 0),
        ([1, {"c": "red", "b": 2}, 3], 4),
        ({"d": "red", "e": [1, 2, 3, 4], "f": 5}, 0),
        ([1, "red", 5], 6),
    ),
)
def test_sum_all_numbers(content, expected):
    assert day_12.sum_all_numbers(content, ignore="red") == expected
