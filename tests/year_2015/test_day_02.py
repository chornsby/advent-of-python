import pytest

from advent.year_2015 import day_02


@pytest.mark.parametrize(
    "content,expected", (("2x3x4", 58), ("1x1x10", 43)),
)
def test_find_paper_area(content, expected):
    assert day_02.find_paper_area(content) == expected


@pytest.mark.parametrize(
    "content,expected", (("2x3x4", 34), ("1x1x10", 14)),
)
def test_find_ribbon_length(content, expected):
    assert day_02.find_ribbon_length(content) == expected
