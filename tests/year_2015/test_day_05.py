import pytest

from advent.year_2015 import day_05


@pytest.mark.parametrize(
    "content,expected",
    (
        ("ugknbfddgicrmopn", 1),
        ("aaa", 1),
        ("jchzalrnumimnmhp", 0),
        ("haegwjzuvuyypxyu", 0),
        ("dvszwmarrgswjxmb", 0),
    ),
)
def test_count_nice_strings_v1(content, expected):
    assert day_05.count_nice_strings_v1(content) == expected


@pytest.mark.parametrize(
    "content,expected",
    (
        ("qjhvhtzxzqqjkmpb", 1),
        ("xxyxx", 1),
        ("uurcxstgmygtbstg", 0),
        ("ieodomkazucvgmuy", 0),
    ),
)
def test_count_nice_strings_v2(content, expected):
    assert day_05.count_nice_strings_v2(content) == expected
