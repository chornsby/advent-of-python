import pytest

from advent.year_2015 import day_06


@pytest.mark.parametrize(
    "content,expected",
    (
        ("turn on 0,0 through 999,999\n", 1_000_000),
        ("toggle 0,0 through 999,0\n", 1_000),
        ("turn off 499,499 through 500,500\n", 0),
    ),
)
def test_count_lights_lit(content, expected):
    assert day_06.count_lights_lit(content) == expected


@pytest.mark.parametrize(
    "content,expected",
    (
        ("turn on 0,0 through 0,0\n", 1),
        ("toggle 0,0 through 999,999\n", 2_000_000),
        ("turn off 499,499 through 500,500\n", 0),
    ),
)
def test_calculate_total_brightness(content, expected):
    assert day_06.calculate_total_brightness(content) == expected
