import pytest

from advent.year_2015 import day_11


@pytest.mark.parametrize(
    "password,expected", (("abcdefgh", "abcdffaa"), ("ghijklmn", "ghjaabcc"))
)
def test_next_valid_password(password, expected):
    assert day_11.next_valid_password(password) == expected
