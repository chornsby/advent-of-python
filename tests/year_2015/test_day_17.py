from advent.year_2015 import day_17


def test_eggnog_combinations():
    assert day_17.count_solutions([20, 15, 10, 5, 5], goal=25) == 4
    assert day_17.count_efficient_solutions([20, 15, 10, 5, 5], goal=25) == 3
