import textwrap

from advent.year_2015 import day_09


def test_find_shortest_path():
    content = textwrap.dedent(
        """
        London to Dublin = 464
        London to Belfast = 518
        Dublin to Belfast = 141
        """
    ).strip()

    assert day_09.find_route_distance(content, choose=min) == 605
    assert day_09.find_route_distance(content, choose=max) == 982
