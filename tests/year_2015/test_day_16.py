import textwrap

from advent.year_2015 import day_16


def test_identify_sue():
    content = textwrap.dedent(
        """
        Sue 1: children: 1, cars: 8, vizslas: 7
        Sue 2: akitas: 10, perfumes: 10, children: 5
        Sue 213: children: 3, goldfish: 5, vizslas: 0
        Sue 323: perfumes: 1, trees: 6, goldfish: 0
        """
    ).strip()
    assert day_16.identify_sue(content) == 213
    assert day_16.identify_sue(content, improved=True) == 323
