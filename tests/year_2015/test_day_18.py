import textwrap

import pytest

from advent.year_2015 import day_18


@pytest.mark.parametrize("steps,expected", ((0, 15), (1, 11), (2, 8), (3, 4), (4, 4)))
def test_count_lights_lit(steps, expected):
    content = textwrap.dedent(
        """
        .#.#.#
        ...##.
        #....#
        ..#...
        #.#..#
        ####..
        """
    ).strip()
    assert day_18.count_lights_lit(content, steps) == expected


@pytest.mark.parametrize(
    "steps,expected", ((0, 17), (1, 18), (2, 18), (3, 18), (4, 14), (5, 17))
)
def test_count_lights_lit_corners_on(steps, expected):
    content = textwrap.dedent(
        """
        ##.#.#
        ...##.
        #....#
        ..#...
        #.#..#
        ####.#
        """
    ).strip()
    assert day_18.count_lights_lit(content, steps, corners_on=True) == expected
