import click

from advent.year_2015.cli import cli as year_2015_cli


@click.group()
def cli():
    """Solve Advent of Code puzzles using Python."""


cli.add_command(year_2015_cli)
