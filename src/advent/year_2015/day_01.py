from typing import TextIO

import click


@click.command("01")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """Not Quite Lisp."""
    content = file_in.read()
    click.echo(find_destination_floor(content))
    click.echo(find_basement_index(content))


def find_destination_floor(content: str) -> int:
    """Find the resulting floor after following all directions."""
    current_floor = 0

    for direction in content:
        if direction == "(":
            current_floor += 1
        elif direction == ")":
            current_floor -= 1
        else:
            raise ValueError(f"Unknown direction: {direction}")

    return current_floor


def find_basement_index(content: str) -> int:
    """Find the index of the first direction to enter the basement."""
    current_floor = 0

    for index, direction in enumerate(content, 1):
        if direction == "(":
            current_floor += 1
        elif direction == ")":
            current_floor -= 1
        else:
            raise ValueError(f"Unknown direction: {direction}")

        if current_floor < 0:
            return index

    raise ValueError(f"Directions never enter the basement")
