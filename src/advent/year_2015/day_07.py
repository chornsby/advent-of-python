import functools
import re
from typing import Dict, NamedTuple, Optional, TextIO, Union

import click

RefOrValue = Union[int, str]

RE_BINARY_OP = re.compile(r"^([a-z0-9]+) ([A-Z]+) ([a-z0-9]+) -> ([a-z0-9]+)$")
RE_UNARY_OP = re.compile(r"^([A-Z]+) ([a-z0-9]+) -> ([a-z0-9]+)$")
RE_ASSIGN_OP = re.compile(r"^([a-z0-9]+) -> ([a-z0-9]+)$")


class Connection(NamedTuple):
    """Represent the incoming connections to a given wire."""

    left: Optional[RefOrValue]
    operator: str
    right: Optional[RefOrValue]


@click.command("07")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """Some Assembly Required."""
    content = file_in.read()
    original_signal = signal_at_wire(content, wire="a")
    overridden_signal = signal_at_wire(content, wire="a", override=original_signal)

    click.echo(original_signal)
    click.echo(overridden_signal)


def signal_at_wire(content: str, wire: str, override: Optional[int] = None) -> int:
    """Simulate the resulting signal at the given wire."""
    connections = build_connections(content, override)

    @functools.lru_cache()
    def recurse(current: RefOrValue) -> int:
        # If we have reached single value then just return it
        if isinstance(current, int):
            return current

        # Otherwise try to resolve the reference
        connection = connections[current]

        if connection.operator == "ASSIGN":
            return recurse(connection.left)
        elif connection.operator == "AND":
            return recurse(connection.left) & recurse(connection.right)
        elif connection.operator == "OR":
            return recurse(connection.left) | recurse(connection.right)
        elif connection.operator == "LSHIFT":
            return recurse(connection.left) << recurse(connection.right)
        elif connection.operator == "RSHIFT":
            return recurse(connection.left) >> recurse(connection.right)
        elif connection.operator == "NOT":
            return ~recurse(connection.left)

        raise ValueError(f"Unknown reference or value: {current}")

    # Convert negative numbers into unsigned 16 bit integers
    return recurse(wire) % (2 ** 16)


def build_connections(content: str, override: Optional[int]) -> Dict[str, Connection]:
    """Build a mapping of wires to their connections from the input data."""
    connections = {}

    for instruction in content.splitlines():
        left, operator, right = None, None, None

        if match := RE_BINARY_OP.match(instruction):
            left, operator, right, target = match.groups()
        elif match := RE_UNARY_OP.match(instruction):
            operator, left, target = match.groups()
        elif match := RE_ASSIGN_OP.match(instruction):
            left, target = match.groups()
            operator = "ASSIGN"
        else:
            raise ValueError(f"Unknown instruction {instruction}")

        if left and left.isdigit():
            left = int(left)
        if right and right.isdigit():
            right = int(right)

        connections[target] = Connection(left, operator, right)

    if override is not None:
        connections["b"] = Connection(left=override, operator="ASSIGN", right=None)

    return connections
