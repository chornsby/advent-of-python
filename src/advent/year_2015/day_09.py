import itertools
import re
from typing import Callable, Dict, FrozenSet, Iterable, Set, TextIO, Tuple

import click
import more_itertools

Legs = Dict[FrozenSet[str], int]
Locations = Set[str]

RE_LEG = re.compile(r"(\w+) to (\w+) = (\d+)")


@click.command("09")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """All in a Single Night."""
    content = file_in.read()
    click.echo(find_route_distance(content, choose=min))
    click.echo(find_route_distance(content, choose=max))


def find_route_distance(content: str, *, choose: Callable[[Iterable[int]], int]) -> int:
    """Return the distance of a route visiting all locations.

    By giving a function like "min" or "max" you can choose which route Santa
    will take this year.
    """
    legs, locations = build_legs_and_locations(content)
    itineraries = itertools.permutations(locations, len(locations))

    def calculate_distance(itinerary: Tuple[str]) -> int:
        """Return the total distance over the given itinerary."""
        return sum(legs[frozenset(pair)] for pair in more_itertools.pairwise(itinerary))

    distances = map(calculate_distance, itineraries)

    return choose(distances)


def build_legs_and_locations(content: str) -> Tuple[Legs, Locations]:
    """Convert the input data into more useful data structures."""
    legs = {}
    locations = set()

    for leg in RE_LEG.finditer(content):
        *pair, distance = leg.groups()
        pair = frozenset(pair)
        distance = int(distance)

        legs[pair] = distance
        locations.update(pair)

    return legs, locations
