from typing import TextIO

import click


@click.command("18")
@click.argument("file-in", type=click.File())
@click.option("--steps", default=100, type=int)
def cli(file_in: TextIO, steps: int):
    """Like a GIF For Your Yard."""
    content = file_in.read().strip()
    click.echo(count_lights_lit(content, steps))
    click.echo(count_lights_lit(content, steps, corners_on=True))


def count_lights_lit(initial_state: str, steps: int, corners_on: bool = False):
    """Return the number of lights lit after running a number of steps."""
    state = {}
    x_max = 0
    y_max = 0

    for y, line in enumerate(initial_state.splitlines()):
        for x, character in enumerate(line):
            x_max = max(x, x_max)
            y_max = max(y, y_max)

            state[(x, y)] = character == "#"

    if corners_on:
        state[0, 0] = state[0, y_max] = state[x_max, 0] = state[x_max, y_max] = True

    for _ in range(steps):
        next_state = {}

        for x in range(x_max + 1):
            for y in range(y_max + 1):
                lit_neighbours = sum(
                    state.get((x + i, y + j), 0)
                    for i in (-1, 0, +1)
                    for j in (-1, 0, +1)
                    if i or j
                )

                if state[x, y]:
                    next_state[x, y] = lit_neighbours == 2 or lit_neighbours == 3
                else:
                    next_state[x, y] = lit_neighbours == 3

        state = next_state

        if corners_on:
            state[0, 0] = state[0, y_max] = state[x_max, 0] = state[x_max, y_max] = True

    return sum(state.values())
