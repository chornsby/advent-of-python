import re
from typing import TextIO

import click

PLACEHOLDER = "_"
RE_ESCAPE = re.compile(r'\\\\|\\"|\\x[0-9a-f]{2}')


@click.command("08")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """Matchsticks."""
    content = file_in.read()
    click.echo(calculate_decode_difference(content))
    click.echo(calculate_encode_difference(content))


def calculate_decode_difference(content: str) -> int:
    """Return the difference between code and memory character count."""
    characters_code = 0
    characters_memory = 0

    for line_in_code in content.splitlines():
        line_in_memory = RE_ESCAPE.sub(PLACEHOLDER, line_in_code)

        characters_code += len(line_in_code)
        characters_memory += len(line_in_memory) - 2

    return characters_code - characters_memory


def calculate_encode_difference(content: str) -> int:
    """Return the difference between encoded and original character counts.

    Here we only need to the number of characters that need encoding and then
    add two for the quotation marks needed for the string literal.
    """
    return sum(line.count('"') + line.count("\\") + 2 for line in content.splitlines())
