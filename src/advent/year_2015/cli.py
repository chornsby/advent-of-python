import importlib

import click


@click.group("2015")
def cli():
    """Tackle the challenges of 2015."""


# Dynamically import all subcommands within the current package
package = importlib.import_module(__name__).__package__

for day in range(25):
    try:
        module = importlib.import_module(f".day_{day + 1:02}", package)
    except ModuleNotFoundError:
        break
    else:
        cli.add_command(module.cli)
