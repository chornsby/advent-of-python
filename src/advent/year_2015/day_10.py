import click
import more_itertools


@click.command("10")
@click.argument("number", type=click.IntRange(min=0))
def cli(number: int):
    """Elves Look, Elves Say."""
    click.echo(look_and_say(number, repetitions=40))
    click.echo(look_and_say(number, repetitions=50))


def look_and_say(number: int, repetitions: int) -> int:
    """Return the length of the number after repetitions of look and say."""
    digits = (int(digit) for digit in str(number))

    for _ in range(repetitions):
        encoded = more_itertools.run_length.encode(digits)
        digits = more_itertools.flatten((count, digit) for digit, count in encoded)

    return more_itertools.ilen(digits)
