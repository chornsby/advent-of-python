import json
from typing import Optional, TextIO, Union

import click


@click.command("12")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """JSAbacusFramework.io."""
    content = json.load(file_in)
    click.echo(sum_all_numbers(content))
    click.echo(sum_all_numbers(content, ignore="red"))


def sum_all_numbers(
    content: Union[dict, int, list, str], ignore: Optional[str] = None
) -> int:
    """Return the sum of all numbers nested in the given JSON structure.

    If the "ignore" parameter is supplied then do not recurse into any JSON
    object that contains the ignored value.
    """
    if isinstance(content, dict):
        if ignore and ignore in content.values():
            return 0
        return sum(sum_all_numbers(value, ignore) for value in content.values())
    elif isinstance(content, list):
        return sum(sum_all_numbers(value, ignore) for value in content)
    elif isinstance(content, int):
        return content
    elif isinstance(content, str):
        return 0
    else:
        raise ValueError(f"Unknown content type: {content}")
