import re
from typing import TextIO

import click
import more_itertools

RE_INSTRUCTION = re.compile(r"([\w\s]+) (\d+),(\d+) through (\d+),(\d+)\n")


@click.command("06")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """Probably a Fire Hazard."""
    content = file_in.read()
    click.echo(count_lights_lit(content))
    click.echo(calculate_total_brightness(content))


def count_lights_lit(content: str) -> int:
    """Return how many lights are lit after following all instructions."""
    lights = [[0 for _ in range(1_000)] for _ in range(1_000)]

    for instruction in RE_INSTRUCTION.finditer(content):
        command, *others = instruction.groups()
        x_min, y_min, x_max, y_max = (int(value) for value in others)

        for x in range(x_min, x_max + 1):
            for y in range(y_min, y_max + 1):
                if command == "turn on":
                    lights[x][y] = 1
                elif command == "turn off":
                    lights[x][y] = 0
                elif command == "toggle":
                    lights[x][y] = 0 if lights[x][y] else 1
                else:
                    raise ValueError(f"Unknown command {repr(command)}")

    return sum(more_itertools.flatten(lights))


def calculate_total_brightness(content: str) -> int:
    """Return the total brightness after following all instructions."""
    lights = [[0 for _ in range(1_000)] for _ in range(1_000)]

    for instruction in RE_INSTRUCTION.finditer(content):
        command, *others = instruction.groups()
        x_min, y_min, x_max, y_max = (int(value) for value in others)

        for x in range(x_min, x_max + 1):
            for y in range(y_min, y_max + 1):
                if command == "turn on":
                    lights[x][y] += 1
                elif command == "turn off":
                    lights[x][y] = max(0, lights[x][y] - 1)
                elif command == "toggle":
                    lights[x][y] += 2
                else:
                    raise ValueError(f"Unknown command {command}")

    return sum(more_itertools.flatten(lights))
