import collections
import itertools
import re
from typing import Dict, Iterator, TextIO

import click
import more_itertools

RE_REINDEER = re.compile(
    r"(\w+) can fly (\d+) km/s for (\d+) seconds, "
    r"but then must rest for (\d+) seconds\."
)


@click.command("14")
@click.argument("file-in", type=click.File())
@click.option("--duration", default=2503, type=int)
def cli(file_in: TextIO, duration: int):
    """Reindeer Olympics."""
    content = file_in.read()
    click.echo(calculate_winning_distance(content, duration))
    click.echo(calculate_winning_points(content, duration))


def calculate_winning_distance(content: str, duration: int) -> int:
    """Return the distance traveled by the winning reindeer."""
    reindeer = build_reindeer(content)
    results = {
        name: more_itertools.nth(participant, duration - 1)
        for name, participant in reindeer.items()
    }
    return max(results.values())


def calculate_winning_points(content: str, duration: int) -> int:
    """Return the number of points earned by the winning reindeer."""
    reindeer = build_reindeer(content)
    points_per_reindeer = collections.defaultdict(int)

    for second in range(duration):
        progress = {
            name: more_itertools.first(participant)
            for name, participant in reindeer.items()
        }
        best = max(progress.values())

        for name, distance in progress.items():
            if distance == best:
                points_per_reindeer[name] += 1

    return max(points_per_reindeer.values())


def build_reindeer(content: str) -> Dict[str, Iterator[int]]:
    """Return generators representing the reindeer's cumulative distance traveled."""
    reindeer = {}

    for match in RE_REINDEER.finditer(content):
        name, *others = match.groups()
        speed, stamina, rest = (int(group) for group in others)

        flying = itertools.repeat(speed, stamina)
        resting = itertools.repeat(0, rest)
        racing = itertools.cycle(itertools.chain(flying, resting))

        reindeer[name] = itertools.accumulate(racing)

    return reindeer
