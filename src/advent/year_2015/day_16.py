import re
from typing import Dict, List, NamedTuple, TextIO

import click
import more_itertools

RE_AUNT = re.compile(r"Sue (\d+): (\w+): (\d+), (\w+): (\d+), (\w+): (\d+)")

REFERENCE = {
    "akitas": 0,
    "cars": 2,
    "cats": 7,
    "children": 3,
    "goldfish": 5,
    "perfumes": 1,
    "pomeranians": 3,
    "samoyeds": 2,
    "trees": 3,
    "vizslas": 0,
}


@click.command("16")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """Aunt Sue."""
    content = file_in.read()
    click.echo(identify_sue(content))
    click.echo(identify_sue(content, improved=True))


def identify_sue(content: str, improved: bool = False) -> int:
    """Return the id of the aunt who sent the present."""
    aunts = build_aunts(content)

    for aunt in aunts:
        for key, value in aunt.sample.items():
            if improved and key in ("cats", "trees"):
                if value <= REFERENCE[key]:
                    break
            elif improved and key in ("goldfish", "pomeranians"):
                if REFERENCE[key] <= value:
                    break
            elif REFERENCE[key] != value:
                break
        else:
            return aunt.identifier

    raise ValueError("No matching aunt found")


class Aunt(NamedTuple):
    """One of my many Aunt Sues who may have sent me the present."""

    identifier: int
    sample: Dict[str, int]


def build_aunts(content: str) -> List[Aunt]:
    """Return all aunts generated from the puzzle input."""
    aunts = []

    for match in RE_AUNT.finditer(content):
        identifier, *rest = match.groups()

        identifier = int(identifier)
        sample = [(key, int(value)) for key, value in more_itertools.grouper(rest, 2)]

        aunt = Aunt(identifier, dict(sample))
        aunts.append(aunt)

    return aunts
