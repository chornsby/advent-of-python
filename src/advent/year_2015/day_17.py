import collections
from typing import List, TextIO

import click


@click.command("17")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """No Such Thing as Too Much."""
    content = file_in.read().strip()
    containers = sorted((int(size) for size in content.splitlines()), reverse=True)

    click.echo(f"Total solutions: {count_solutions(containers)}")
    click.echo(f"Efficient solutions: {count_efficient_solutions(containers)}")


def count_solutions(containers: List[int], goal: int = 150) -> int:
    """Return the number of combinations of containers for the eggnog."""
    combinations = generate_combinations(goal=goal, available=containers, used=[])
    return len(combinations)


def count_efficient_solutions(containers: List[int], goal: int = 150) -> int:
    """Return the number of the most efficient solutions in terms of space."""
    combinations = generate_combinations(goal=goal, available=containers, used=[])
    counter = collections.Counter(len(solution) for solution in combinations)
    return counter[min(counter.keys())]


def generate_combinations(*, goal, available, used) -> List[List[int]]:
    """Recursively generate a list of all suitable combinations of container."""
    if goal == 0:
        return [used]

    return sum(
        (
            generate_combinations(
                goal=goal - available[i],
                available=available[i + 1 :],
                used=used + available[i : i + 1],
            )
            for i in range(len(available))
            if 0 <= goal - available[i]
        ),
        [],
    )
