from typing import TextIO

import click


@click.command("02")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """I Was Told There Would Be No Math."""
    content = file_in.read()
    click.echo(find_paper_area(content))
    click.echo(find_ribbon_length(content))


def find_paper_area(content: str) -> int:
    """Find the exact area of wrapping paper to order."""
    total_area = 0

    for present in content.splitlines():
        h, l, w = sorted(int(dimension) for dimension in present.split("x"))

        area = (2 * l * w) + (2 * w * h) + (2 * h * l)
        slack = h * l

        total_area += area + slack

    return total_area


def find_ribbon_length(content: str) -> int:
    """Find the exact length of ribbon to order."""
    total_length = 0

    for present in content.splitlines():
        h, l, w = sorted(int(dimension) for dimension in present.split("x"))

        length = 2 * h + 2 * l
        bow = h * l * w

        total_length += length + bow

    return total_length
