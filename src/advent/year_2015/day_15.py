import itertools
import re
from typing import Iterator, NamedTuple, TextIO, Tuple

import click

RE_INGREDIENT = re.compile(
    r"(\w+): capacity (-?\d+), durability (-?\d+), "
    r"flavor (-?\d+), texture (-?\d+), calories (-?\d+)"
)


class Ingredient(NamedTuple):
    """A single ingredient for use in baking cookies."""

    name: str
    capacity: int
    durability: int
    flavor: int
    texture: int
    calories: int


@click.command("15")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """Science for Hungry People."""
    content = file_in.read()
    click.echo(find_optimal_cookie_score(content))
    click.echo(find_optimal_cookie_score(content, require_calories=500))


def find_optimal_cookie_score(content: str, require_calories: int = None) -> int:
    """Return the total score of the highest scoring cookie."""
    recipes = build_recipes(content)

    return max(calculate_score(recipe, require_calories) for recipe in recipes)


def build_recipes(content: str) -> Iterator[Tuple[Ingredient]]:
    """Return all combinations of ingredients for all possible recipes."""
    ingredients = []

    for match in RE_INGREDIENT.finditer(content):
        name, *rest = match.groups()
        attributes = (int(attribute) for attribute in rest)

        ingredient = Ingredient(name, *attributes)
        ingredients.append(ingredient)

    return itertools.combinations_with_replacement(ingredients, 100)


def calculate_score(recipe: Tuple[Ingredient], require_calories: int = None) -> int:
    """Return the non-negative score for the given ingredients."""
    if require_calories is not None:
        calories = max(0, sum(ingredient.calories for ingredient in recipe))

        if calories != require_calories:
            return 0

    capacity = max(0, sum(ingredient.capacity for ingredient in recipe))
    durability = max(0, sum(ingredient.durability for ingredient in recipe))
    flavor = max(0, sum(ingredient.flavor for ingredient in recipe))
    texture = max(0, sum(ingredient.texture for ingredient in recipe))

    return capacity * durability * flavor * texture
