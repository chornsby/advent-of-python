from typing import List

import click
import more_itertools

FORBIDDEN_ORDINALS = {ord("i"), ord("o"), ord("l")}

ORDINAL_A = ord("a")
ORDINAL_OVERFLOW = ord("z") + 1


@click.command("11")
@click.argument("password")
def cli(password: str):
    """Corporate Policy."""
    next_password = next_valid_password(password)
    click.echo(next_password)

    following_password = next_valid_password(next_password)
    click.echo(following_password)


def next_valid_password(password: str) -> str:
    """Return the next valid password after incrementing the previous one."""
    password = [ord(char) for char in password]

    while True:
        # Increment the password
        password[-1] += 1

        # Skip testing forbidden letters as a critical optimisation
        for index in range(len(password)):
            if password[index] in FORBIDDEN_ORDINALS:
                password[index] += 1
                password[index + 1 :] = (
                    ORDINAL_A for _ in range(index + 1, len(password))
                )

        # Resolve overflows
        for index in reversed(range(1, len(password))):
            if password[index] == ORDINAL_OVERFLOW:
                password[index] = ORDINAL_A
                password[index - 1] += 1
            else:
                break
        else:
            password = [ORDINAL_A] + password

        # Stop incrementing the password if it is now valid
        if is_valid(password):
            break

    return "".join(chr(ordinal) for ordinal in password)


def is_valid(password: List[int]) -> bool:
    """Return True if the password is valid based on the Security-Elf rules."""
    # Passwords must contain at least two different non-overlapping pairs
    encoded = more_itertools.run_length.encode(password)
    duplicates = (ordinal for ordinal, count in encoded if 2 <= count)

    if more_itertools.ilen(duplicates) < 2:
        return False

    # Passwords must include one increasing straight of at least three letters
    triples = more_itertools.windowed(password, 3)

    if not any(y == x + 1 and z == y + 1 for x, y, z in triples):
        return False

    return True
