import itertools
import re
from typing import Dict, Set, TextIO, Tuple

import click
import more_itertools

Guests = Set[str]
Relationships = Dict[Tuple[str, str], int]

RE_SOCIAL_DATA = re.compile(
    r"(\w+) would (\w+) (\d+) happiness units by sitting next to (\w+)\."
)


@click.command("13")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """Knights of the Dinner Table."""
    content = file_in.read()
    click.echo(find_optimal_happiness(content))
    click.echo(find_optimal_happiness(content, include_self=True))


def find_optimal_happiness(content: str, include_self: bool = False) -> int:
    """Return the maximum change in happiness due to the seating arrangement."""
    guests, relationships = build_guests_and_relationships(content)

    # Add ambivalent relationships with one additional guest if included
    if include_self:
        for guest in guests:
            relationships[(guest, "Yourself")] = 0
            relationships[("Yourself", guest)] = 0

        guests.add("Yourself")

    # Generate all possible seating arrangements
    arrangements = itertools.permutations(guests, len(guests))

    def calculate_happiness(arrangement: Tuple[str]) -> int:
        """Return the total happiness for the given arrangement."""
        happiness = 0

        for x, y in more_itertools.pairwise(arrangement):
            happiness += relationships[(x, y)]
            happiness += relationships[(y, x)]

        # Remember to include the relationship between the first and last guest
        happiness += relationships[(arrangement[0], arrangement[-1])]
        happiness += relationships[(arrangement[-1], arrangement[0])]

        return happiness

    return max(calculate_happiness(arrangement) for arrangement in arrangements)


def build_guests_and_relationships(content: str) -> Tuple[Guests, Relationships]:
    """Convert the input data into more useful data structures."""
    guests = set()
    relationships = {}

    for match in RE_SOCIAL_DATA.finditer(content):
        from_, sign, value, to = match.groups()

        if sign == "gain":
            value = int(value)
        elif sign == "lose":
            value = -int(value)
        else:
            raise ValueError(f"Unknown sign: {sign}")

        guests.add(from_)
        guests.add(to)

        relationships[(from_, to)] = value

    return guests, relationships
