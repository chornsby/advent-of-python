import itertools
from typing import TextIO

import click


@click.command("03")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """Perfectly Spherical Houses in a Vacuum."""
    content = file_in.read()
    click.echo(count_present_deliveries(content, num_santas=1))
    click.echo(count_present_deliveries(content, num_santas=2))


def count_present_deliveries(content: str, num_santas: int) -> int:
    """Return how many houses receive at least one present."""
    houses_visited = {(0, 0)}
    santa_positions = [(0, 0) for _ in range(num_santas)]

    turns = itertools.cycle(range(num_santas))

    for direction, santa_index in zip(content, turns):
        if direction == "^":
            vector = (0, 1)
        elif direction == "v":
            vector = (0, -1)
        elif direction == ">":
            vector = (1, 0)
        elif direction == "<":
            vector = (-1, 0)
        else:
            raise ValueError(f"Unknown direction {direction}")

        current_position = santa_positions[santa_index]
        new_position = tuple(sum(x) for x in zip(current_position, vector))

        santa_positions[santa_index] = new_position

        houses_visited.add(new_position)

    return len(houses_visited)
