import hashlib
import itertools

import click


@click.command("04")
@click.argument("secret-key")
def cli(secret_key: str):
    """The Ideal Stocking Stuffer."""
    click.echo(mine_advent_coin(secret_key, leading_zeroes=5))
    click.echo(mine_advent_coin(secret_key, leading_zeroes=6))


def mine_advent_coin(secret_key: str, leading_zeroes: int) -> int:
    """Return the lowest number to make a hash with the given leading zeroes."""
    target_prefix = "0" * leading_zeroes
    md5 = hashlib.md5(secret_key.encode())

    for suffix in itertools.count():
        digest = md5.copy()
        digest.update(b"%d" % suffix)

        if digest.hexdigest().startswith(target_prefix):
            return suffix
