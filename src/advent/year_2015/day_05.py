import collections
from typing import TextIO

import click
import more_itertools


@click.command("05")
@click.argument("file-in", type=click.File())
def cli(file_in: TextIO):
    """Doesn't He Have Intern-Elves For This?"""
    content = file_in.read()
    click.echo(count_nice_strings_v1(content))
    click.echo(count_nice_strings_v2(content))


def count_nice_strings_v1(content: str) -> int:
    """Return the number of nice strings given Santa's original rules."""
    count = 0
    forbidden_substrings = ("ab", "cd", "pq", "xy")
    vowels = ("a", "e", "i", "o", "u")

    for string in content.splitlines():
        counter = collections.Counter(string)

        # It contains at least three vowels
        if sum(counter.get(vowel, 0) for vowel in vowels) < 3:
            continue

        # It contains at least one letter that appears twice in a row
        if not any(x == y for x, y in more_itertools.pairwise(string)):
            continue

        # It does not contain any of the forbidden substrings
        if any(substring in string for substring in forbidden_substrings):
            continue

        count += 1

    return count


def count_nice_strings_v2(content: str) -> int:
    """Return the number of nice strings given Santa's improved rules."""
    count = 0

    for string in content.splitlines():
        # It contains a pair of any two letters that appears at least twice in
        # the string without overlapping
        if all(string.count(x + y) < 2 for x, y in more_itertools.pairwise(string)):
            continue

        # It contains at least one letter which repeats with exactly one letter
        # between them
        if not any(x == z for x, _, z in more_itertools.windowed(string, 3)):
            continue

        count += 1

    return count
